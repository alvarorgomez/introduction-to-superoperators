# Introduction to Superoperators

[Course Start](START.ipynb) &nbsp;|&nbsp;

## Bibliography

Quantum Computation and Quantum Information - 10th Anniversary Edition - Michael A. Nielsen & Isaac L. Chuang

An Introduction to Quantum Computing - Phillip Kaye & Raymond Laflamme & Michele Mosca

